import path from 'path';
import { validate as uuidValidate } from 'uuid';
import CartParser from './CartParser';
import cartJson from '../samples/cart.json';

let parser;

expect.extend({
  toBeUuid(id) {
    if (uuidValidate(id)) {
      return {
        message: () => `${id} is valid uuid`,
        pass: true,
      };
    } else {
      return {
        message: () => `${id} is not valid uuid`,
        pass: false,
      };
    }
  },
});

beforeEach(() => {
  parser = new CartParser();
});

describe('parse', () => {
  it('should throw Error when is not valid path', () => {
    expect(() => {
      parser.parse('');
    }).toThrow('ENOENT: no such file or directory, open');
  });

  it('should throw Error when is not valid content', () => {
    parser.readFile = jest.fn(() => '');
    parser.validate = jest.fn(() => [{}, {}]);
    expect(() => {
      parser.parse('');
    }).toThrow('Validation failed!');
  });
});

describe('validate', () => {
  it('should return empty array when headers is "Product name, Price, Quantity"', () => {
    const contents = 'Product name,Price,Quantity';
    expect(parser.validate(contents)).toEqual([]);
  });

  it('should return array with one error when first header name is not "Product name"', () => {
    const contents = 'Product nam,Price,Quantity';
    expect(parser.validate(contents)).toEqual([
      {
        column: 0,
        message: 'Expected header to be named "Product name" but received Product nam.',
        row: 0,
        type: 'header',
      },
    ]);
  });

  it('should return array with one error when second header name is not "Price"', () => {
    const contents = 'Product name,Pric,Quantity';
    expect(parser.validate(contents)).toEqual([
      {
        column: 1,
        message: 'Expected header to be named "Price" but received Pric.',
        row: 0,
        type: 'header',
      },
    ]);
  });

  it('should return array with one error when second header name is not "Quantity"', () => {
    const contents = 'Product name,Price,Quantit';
    expect(parser.validate(contents)).toEqual([
      {
        column: 2,
        message: 'Expected header to be named "Quantity" but received Quantit.',
        row: 0,
        type: 'header',
      },
    ]);
  });

  it('should return empty array when count cell more than 2 and type cells is valid', () => {
    const contents = `Product name,Price,Quantity
                      Mollis consequat,9.00,2`;
    expect(parser.validate(contents)).toEqual([]);
  });

  it('should return array with one error when count cell less than 3', () => {
    const contents = `Product name,Price,Quantity
                      Mollis consequat,9.00`;
    expect(parser.validate(contents)).toEqual([
      {
        column: -1,
        message: 'Expected row to have 3 cells but received 2.',
        row: 1,
        type: 'row',
      },
    ]);
  });

  it('should return array with one error when cell with string type is empty', () => {
    const contents = `Product name,Price,Quantity
                      ,9.00,2`;
    expect(parser.validate(contents)).toEqual([
      {
        column: 0,
        message: 'Expected cell to be a nonempty string but received "".',
        row: 1,
        type: 'cell',
      },
    ]);
  });

  it('should return array with one error when cell with number type is NaN', () => {
    const contents = `Product name,Price,Quantity
                      Mollis consequat,9.00,one`;
    expect(parser.validate(contents)).toEqual([
      {
        column: 2,
        message: 'Expected cell to be a positive number but received "one".',
        row: 1,
        type: 'cell',
      },
    ]);
  });

  it('should return array with one error when cell with number type is negative', () => {
    const contents = `Product name,Price,Quantity
                      Mollis consequat,9.00,-1`;
    expect(parser.validate(contents)).toEqual([
      {
        column: 2,
        message: 'Expected cell to be a positive number but received "-1".',
        row: 1,
        type: 'cell',
      },
    ]);
  });
});

describe('CartParser - integration test', () => {
  it('should return object cart when argument path is path to csv file', () => {
    const cart = {
      items: cartJson.items.map(({ name, price, quantity }) => ({
        id: expect.toBeUuid(),
        name,
        price,
        quantity,
      })),
      total: cartJson.total,
    };
    const file = '../samples/cart.csv';
    const pathFile = path.resolve(__dirname, file);
    expect(parser.parse(pathFile)).toMatchObject(cart);
  });
});
